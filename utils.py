# coding=utf-8
# /usr/bin/python -tt

import os, errno
import re
import requests
import custom_logger as logger

from mimetypes import guess_type, guess_extension
from urlparse import urlparse
from threading import Thread
from urllib import urlretrieve
from bs4 import BeautifulSoup


def download(url):
    file_type = check_mime_content(url, check=True)
    location = './downloads/{}/'.format(file_type)
    logger.info('{0} -> {1}'.format('saving', url))
    split_path = url.split('/')
    name = split_path[-1] if len(split_path[-1].split('.')) > 1 else None
    path = '{0}{1}/'.format(location, '/'.join([_ for _ in split_path[1:-1] if _]))

    if name and check_mime_content(url):
        try:
            create_folders(path)
            saved_path, meta = urlretrieve(url, path + name)
            content_type = meta['Content-Type']

            if check_mime_content(saved_path, check=True) != content_type:
                logger.warn(url, {"level":"download"})
                os.remove(saved_path)
        except Exception:
            pass


def create_folders(path):
    try:
        os.makedirs(path)
    except OSError, e:
        if e.errno != errno.EEXIST:
            logger.error(e)
        raise e


def compare_hosts(start, stop):
    if not urlparse(start).netloc:
        raise ValueError('Wrong URL')

    start_page = map(str, urlparse(start).netloc.split('.'))
    current_page = map(str, urlparse(stop).netloc.split('.'))

    start_host, current_host = zip(start_page, current_page)[1]
    return start_host == current_host


def get_all_links(page):
    links = [url.get('href') for url in page.findAll('a', attrs={'href': re.compile("^http://")}) if url]

    return set(links)


def in_parallel(fn, list_to_process):
    for item in list_to_process:
        try:
            Thread(target=fn, args=(item,)).start()
        except Exception, e:
            logger.error(item)


def check_mime_content(data_url, check=False):
    mime, _ = guess_type(data_url)
    if check:
        return mime
    else:
        return True if mime else False


def custom_opener(url, text=False):
    try:
        get = requests.get(url).text
        soup = BeautifulSoup(get, 'html.parser')
        return soup if not text else get

    except Exception, e:
        logger.error(e)
        raise e
