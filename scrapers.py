# coding=utf-8
# /usr/bin/python -tt
import re

def get_documents(page):
    p = re.compile(ur'src=\"(.*?\.pdf)"')
    docs = re.findall(p, page.text)
    docs = map(str, docs) # unicode problems
    docs = ['http:{}'.format(doc) for doc in docs if 'http' not in doc]

    return set(docs)


def get_videos(page):
    videos = page.find_all('div', {'id': 'fit-product-jwplayer'})
    videos = [str(video.get('data-file')) for video in videos if video]
    return set(videos)


def get_images(page):
    images = page.find_all('img')
    images = [str(image.get('src')) for image in images if image]
    images = ['http:{}'.format(image) for image in images if 'http' not in image]
    return set(images)

#1
#2
#3

#4
#5
