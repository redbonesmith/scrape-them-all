# USAGE #

```sh
$ python download.py 'https://www.suitsupply.com'
```


# DONE:

* will download in parallels all images/videos/docs
* mimetype checking
* will save them according to their path into same folder structure:
  * f.e. image `http://statics.suitsupply.com/images/new-homepage/navigation/company/montreal-01.jpg` will be saved to path:
     [`./downloads/images/statics.suitsupply.com/images/new-homepage/navigation/company/montreal-01.jpg`]

# TODO:
