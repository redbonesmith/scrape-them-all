# coding=utf-8
# /usr/bin/python -tt

import scrapers
import optparse

from utils import custom_opener, in_parallel, compare_hosts, get_all_links, download


def main():
    base_page_url = ''

    
    def scrape(url_to_scrape):
        if compare_hosts(base_page_url, url_to_scrape):
            page = custom_opener(url_to_scrape)
            all_links = get_all_links(page)

            for link in all_links:
                sub_page = custom_opener(link)

                videos = scrapers.get_videos(sub_page)
                images = scrapers.get_images(sub_page)
                documents = scrapers.get_documents(sub_page)

                in_parallel(download, videos)
                in_parallel(download, images)
                in_parallel(download, documents)

                scrape(link)
        else:
            return

    parser = optparse.OptionParser()
    options, args = parser.parse_args()
    base_page_url = args[0]

    scrape(base_page_url)

# main scraper
if __name__ == "__main__":
    main()
