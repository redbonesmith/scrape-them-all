import logging

FORMAT = "%(asctime)-15s %(message)s"
logging.basicConfig(format=FORMAT)

def configure_logging():
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)


def warn(msg, data=None):
    logging.warning("[Warning]: %s", msg, extra=data)


def info(msg):
    configure_logging()
    logging.info("[Info]: {}".format(msg))


def error(msg, data=None):
    logging.error("[Error]: %s", msg, extra=data)
